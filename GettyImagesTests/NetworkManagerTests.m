//
//  NetworkManagerTests.m
//  GettyImages
//
//  Created by Maxim Makhun on 9/23/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

@import XCTest;

// Managers
#import "NetworkManager.h"
#import "URLCacheManager.h"

@interface NetworkManagerTests : XCTestCase

@property (nonatomic, strong) NSString *googleEndpoint;

@end

@implementation NetworkManagerTests

- (void)setUp {
    [super setUp];
    
    self.googleEndpoint = @"https://www.google.com";
}

- (void)tearDown {
    [super tearDown];
}

- (void)testThat_networkManager_shouldReceiveNonNilResponse {
    // given
    XCTestExpectation *expectation = [self expectationWithDescription:@"Perform simple request."];
    
    // when
    [[NetworkManager sharedInstance] dataWithAddress:self.googleEndpoint
                                   completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                       // then
                                       XCTAssertNotNil(data, @"Data should have non nil value.");
                                       
                                       XCTAssertNil(error, @"Request should end up without an error.");
                                       
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                       XCTAssertTrue(httpResponse.statusCode == 200, @"Response status code should be 200 - OK.");
                                       
                                       [expectation fulfill];
                                   }];
    
    
    [self waitForExpectationsWithTimeout:10.0f handler:^(NSError *error) {
        XCTAssertNil(error, @"Liked tracks resource is not accessible.");
    }];
}

- (void)testThat_networkManager_shouldHandleCacheReuse {
    // given
    XCTestExpectation *expectation = [self expectationWithDescription:@"Perform simple request."];
    
    __block NSURLSessionTask *task = nil;
    task = [[NetworkManager sharedInstance] dataWithAddress:self.googleEndpoint
                                          completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                              XCTAssertNil(error, @"Request should end up without an error.");
                                              
                                              NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                              XCTAssertTrue(httpResponse.statusCode == 200, @"Response status code should be 200 - OK.");
                                              
                                              // check if response data was cached
                                              NSData *cachedData = [[URLCacheManager sharedInstance] getCachedDataForTask:task];
                                              XCTAssertNotNil(cachedData, @"Cached data should have non nil value.");
                                              
                                              // remove all cached response data
                                              [[URLCacheManager sharedURLCache] removeAllCachedResponses];
                                              
                                              // check if cache was removed
                                              cachedData = [[URLCacheManager sharedInstance] getCachedDataForTask:task];
                                              XCTAssertNil(cachedData, @"Cached data should be nil.");
                                              
                                              [expectation fulfill];
                                          }];
    
    // then
    [self waitForExpectationsWithTimeout:10.0f handler:^(NSError *error) {
        XCTAssertNil(error, @"Google is not accessible.");
    }];
}

@end