//
//  AppDelegate.m
//  GettyImages
//
//  Created by Maxim Makhun on 9/20/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

#import "AppDelegate.h"

// View Controllers
#import "ImagesViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:UIScreen.mainScreen.bounds];
    self.window.rootViewController = [[UINavigationController alloc] initWithRootViewController:[ImagesViewController new]];
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
