//
//  DataFetchManager.m
//  GettyImages
//
//  Created by Maxim Makhun on 9/22/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

#import "DataFetchManager.h"

// Models
#import "GettyImage.h"

// Managers
#import "NetworkManager.h"

// Builders
#import "GettyImageBuilder.h"

@implementation DataFetchManager

+ (instancetype)sharedInstance {
    static id sharedInstance;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [DataFetchManager new];
    });
    
    return sharedInstance;
}

- (void)loadGettyImages:(NSString *)address
      completionHandler:(void (^)(NSArray * __nullable gettyImages, NSError * __nullable error))completionHandler {
    NSLog(@"%@ : %s : called.", NSStringFromClass(self.class), __func__);
    
    [[NetworkManager sharedInstance] dataWithAddress:address
                                   completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                       NSArray *gettyImages = [GettyImageBuilder gettyImagesFromJSON:data error:&error];
                                       completionHandler(gettyImages, error);
                                   }];
}

@end
