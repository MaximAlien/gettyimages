//
//  URLCacheManager.h
//  GettyImages
//
//  Created by Maxim Makhun on 9/22/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

@import Foundation;

@interface URLCacheManager : NSURLCache

/**
 *  URLCacheManager works as singleton. Ths is one and only access point to it.
 *  Cache will store maximum of 20 Megabytes of Disk and Memory capacity.
 *
 *  @return URLCacheManager instance.
 */
+ (instancetype)sharedInstance;

/**
 *  Method allows to cache response data for sepcific NSURLSessionTask.
 *
 *  @param task instance of NSURLSessionTask
 *  @param data     NSData instance which will cached
 */
- (void)cacheDataForTask:(NSURLSessionTask *)task data:(NSData *)data;

/**
 *  Method allows to retrieve cached data for sepcific NSURLSessionTask.
 *
 *  @param task instance of NSURLSessionTask
 *
 *  @return NSData instance which was found in cache
 */
- (NSData *)getCachedDataForTask:(NSURLSessionTask *)task;

@end
