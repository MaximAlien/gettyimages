//
//  DataFetchManager.h
//  GettyImages
//
//  Created by Maxim Makhun on 9/22/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

@import Foundation;

NS_ASSUME_NONNULL_BEGIN

@interface DataFetchManager : NSObject

/**
 *  DataFetchManager works as singleton. Ths is one and only access point to it.
 *  This class works as fetcher for endpoints by using NetworkManager. All returned data
 *  will be parsed and returned in object model form.
 *
 *  @return DataFetchManager instance.
 */
+ (instancetype)sharedInstance;

- (void)loadGettyImages:(NSString *)address
      completionHandler:(void (^)(NSArray * __nullable gettyImages, NSError * __nullable error))completionHandler;

@end

NS_ASSUME_NONNULL_END