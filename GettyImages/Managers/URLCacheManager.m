//
//  URLCacheManager.m
//  GettyImages
//
//  Created by Maxim Makhun on 9/22/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

#import "URLCacheManager.h"

static NSString * const kExpirationKey = @"expiration_key";
static const int kCacheSize = 20 * 1024 * 1024;

@implementation URLCacheManager

#pragma mark - Initialization methods

+ (URLCacheManager *)sharedInstance {
    static id sharedInstance;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [URLCacheManager new];
    });
    
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        URLCacheManager *urlCache = [[URLCacheManager alloc] initWithMemoryCapacity:kCacheSize
                                                                       diskCapacity:kCacheSize
                                                                           diskPath:nil];
        [NSURLCache setSharedURLCache:urlCache];
    }
    
    return self;
}

#pragma mark - Cache read/write methods

- (NSCachedURLResponse *)cachedResponseForRequest:(NSURLRequest *)request {
    NSCachedURLResponse *cachedResponse = [super cachedResponseForRequest:request];
    
    return cachedResponse;
}

- (void)storeCachedResponse:(NSCachedURLResponse *)cachedResponse forRequest:(NSURLRequest *)request {
    NSMutableDictionary *userInfo = cachedResponse.userInfo ? [cachedResponse.userInfo mutableCopy] : [NSMutableDictionary dictionary];
    [userInfo setObject:[NSDate date] forKey:kExpirationKey];
    
    NSCachedURLResponse *newCachedResponse = [[NSCachedURLResponse alloc] initWithResponse:cachedResponse.response
                                                                                      data:cachedResponse.data
                                                                                  userInfo:userInfo
                                                                             storagePolicy:cachedResponse.storagePolicy];
    
    [super storeCachedResponse:newCachedResponse forRequest:request];
}

#pragma mark - Caching helper methods


- (void)cacheDataForTask:(NSURLSessionTask *)task data:(NSData *)data {
    NSLog(@"%@ : %s NSURLSessionTask(%lu) will be cached.", NSStringFromClass(self.class), __func__, (unsigned long)task.taskIdentifier);
    NSCachedURLResponse *cachedResponse = [[NSCachedURLResponse alloc] initWithResponse:task.response
                                                                                   data:data];
    
    [[URLCacheManager sharedURLCache] storeCachedResponse:cachedResponse
                                               forRequest:task.currentRequest];
}

- (NSData *)getCachedDataForTask:(NSURLSessionTask *)task {
    NSLog(@"%@ : %s attemt to retrieve cache for NSURLSessionTask(%lu).", NSStringFromClass(self.class), __func__, (unsigned long)task.taskIdentifier);
    return [[URLCacheManager sharedURLCache] cachedResponseForRequest:task.currentRequest].data;
}

@end