//
//  NetworkManager.m
//  GettyImages
//
//  Created by Maxim Makhun on 9/22/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

#import "NetworkManager.h"

// Frameworks
@import UIKit;

// Managers
#import "URLCacheManager.h"

static const int kTimeoutInterval = 10.0f;
static NSString * const kApiKey = @"4x3mqfykgft2uj2zynnw4b9w";

@implementation NetworkManager

#pragma mark - Initialization methods

+ (instancetype)sharedInstance {
    static id sharedInstance;
    static dispatch_once_t oncePredicate;
    
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [NetworkManager new];
    });
    
    return sharedInstance;
}

+ (NSURLSessionConfiguration *)sessionConfiguration {
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    sessionConfiguration.requestCachePolicy = NSURLRequestReloadIgnoringLocalCacheData;
    sessionConfiguration.timeoutIntervalForRequest = kTimeoutInterval;
    sessionConfiguration.HTTPAdditionalHeaders = @{@"Api-Key" : kApiKey};
    
    return sessionConfiguration;
}

+ (NSURLSession *)session {
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NetworkManager sessionConfiguration]
                                                          delegate:nil
                                                     delegateQueue:nil];
    
    return session;
}

#pragma mark - Network request methods

- (NSURLSessionTask *)dataWithAddress:(NSString *)address
                    completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler {
    NSLog(@"%@ : %s : called.", NSStringFromClass(self.class), __func__);
    NSURL *requestURL = [NSURL URLWithString:address];
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    __block NSURLSessionTask *task = nil;
    task = [[NetworkManager session] dataTaskWithURL:requestURL
                                   completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                       [self handleResultForTask:task
                                                            data:data
                                                        response:response
                                                           error:error
                                               completionHandler:completionHandler];
                                   }];
    [task resume];
    
    return task;
}

#pragma mark - Result handler methods

- (void)handleResultForTask:(NSURLSessionTask *)task
                       data:(NSData *)data
                   response:(NSURLResponse *)response
                      error:(NSError *)error
          completionHandler:(void (^)(NSData *data, NSURLResponse *response, NSError *error))completionHandler {
    dispatch_async(dispatch_get_main_queue(), ^{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        __block NSData *localData = data;
        if (!error) {
            NSLog(@"%@ : %s received response.", NSStringFromClass(self.class), __func__);
            [[URLCacheManager sharedInstance] cacheDataForTask:task data:localData];
        } else {
            NSLog(@"%@ : %s received response with an error: %@.", NSStringFromClass(self.class), __func__, [error localizedDescription]);
            localData = [[URLCacheManager sharedInstance] getCachedDataForTask:task];
        }
        
        completionHandler(localData, response, error);
    });
}

@end
