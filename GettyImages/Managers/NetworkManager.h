//
//  NetworkManager.h
//  GettyImages
//
//  Created by Maxim Makhun on 9/22/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

@import Foundation;

NS_ASSUME_NONNULL_BEGIN

@interface NetworkManager : NSObject

/**
 *  NetworkManager works as singleton. Ths is one and only access point to it.
 *  NetworkManager provides specific NSURLSessionConfiguration and NSURLSession
 *  configurations (request timeouts etc).
 *
 *  @return URLCacheManager instance.
 */
+ (instancetype)sharedInstance;

/**
 *  Method allows to send network request.
 *
 *  @param address           Endpoint address in form of NSString.
 *  @param completionHandler block which returns three major items:
 *  - NSData* instance with response body
 *  - NSURLResponse* instance which holds response headers, status code etc
 *  - NSError* error object in case of any errors
 *
 *  @return NSURLSessionTask instance which holds current task
 */
- (NSURLSessionTask *)dataWithAddress:(NSString *)address
                    completionHandler:(void (^)(NSData * __nullable data, NSURLResponse * __nullable response, NSError * __nullable error))completionHandler;

@end

NS_ASSUME_NONNULL_END