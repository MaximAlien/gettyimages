//
//  UIImageView+DataTask.m
//  GettyImages
//
//  Created by Maxim Makhun on 9/22/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

#import "UIImageView+DataTask.h"

// Runtime
#import <objc/runtime.h>

@implementation UIImageView (DataTask)

- (NSURLSessionDataTask *)dataTask {
    return objc_getAssociatedObject(self, @selector(dataTask));
}

- (void)setDataTask:(NSURLSessionDataTask *)dataTask {
    objc_setAssociatedObject(self, @selector(dataTask), dataTask, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)setImageWithURL:(NSURL *)imageURL {
    if (self.dataTask) {
        [self.dataTask cancel];
    }
    
    if (imageURL) {
        NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
        self.dataTask = [session dataTaskWithURL:imageURL
                               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                   if (!error) {
                                       NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
                                       if (httpResponse.statusCode == 200) {
                                           UIImage *image = [UIImage imageWithData:data];
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               self.image = image;
                                           });
                                       } else {
                                           NSLog(@"Issue when loading image at URL: %@", imageURL);
                                       }
                                   } else {
                                       // log error only for requests which were not cancelled
                                       if (error.code != -999) {
                                           NSLog(@"%s. Error occured: %@", __func__, [error localizedDescription]);
                                       }
                                   }
                               }];
        [self.dataTask resume];
    }
}

@end
