//
//  UIColor+Branding.m
//  GettyImages
//
//  Created by Maxim Makhun on 9/22/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

#import "UIColor+Branding.h"

@implementation UIColor (Branding)

+ (instancetype)getty_blue {
    return [UIColor colorWithRed:0.0f / 255.0f
                           green:150.0f / 255.0f
                            blue:255.0f / 255.0f
                           alpha:1.0f];
}

@end
