//
//  UIColor+Branding.h
//  GettyImages
//
//  Created by Maxim Makhun on 9/22/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

@import UIKit;

@interface UIColor (Branding)

+ (instancetype)getty_blue;

@end
