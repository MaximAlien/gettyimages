//
//  AppDelegate.h
//  GettyImages
//
//  Created by Maxim Makhun on 9/20/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

@import UIKit;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

