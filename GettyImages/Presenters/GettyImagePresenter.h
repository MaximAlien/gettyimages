//
//  GettyImagePresenter.h
//  GettyImages
//
//  Created by Maxim Makhun on 9/22/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

@import Foundation;

@class GettyImage;
@class GettyImageCollectionViewCell;

@interface GettyImagePresenter : NSObject

- (instancetype)initWithModel:(GettyImage *)model view:(GettyImageCollectionViewCell *)view;

- (void)updateView;

@end
