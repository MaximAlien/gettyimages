//
//  GettyImagePresenter.m
//  GettyImages
//
//  Created by Maxim Makhun on 9/22/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

#import "GettyImagePresenter.h"

// Models
#import "GettyImage.h"

// Views
#import "GettyImageCollectionViewCell.h"

// Categories
#import "UIImageView+DataTask.h"

@interface GettyImagePresenter ()

@property (nonatomic, weak) GettyImageCollectionViewCell *view;
@property (nonatomic, weak) GettyImage *model;

@end

@implementation GettyImagePresenter

- (instancetype)initWithModel:(GettyImage *)model view:(GettyImageCollectionViewCell *)view {
    if (self = [super init]) {
        self.model = model;
        self.view = view;
    }
    
    return self;
}

- (void)updateView {
    if ([self.model.uri isKindOfClass:[NSString class]]) {
        NSString *encodedStr = [self.model.uri stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        NSURL *url = [NSURL URLWithString:encodedStr];
        [self.view.imageView setImageWithURL:url];
    }
    
    if ([self.model.ID isKindOfClass:[NSString class]] && [self.model.title isKindOfClass:[NSString class]]) {
        [self.view.titleLabel setText:[NSString stringWithFormat:@" %@\n %@", self.model.ID, self.model.title]];
    }
}

@end
