//
//  ImagesViewController.m
//  GettyImages
//
//  Created by Maxim Makhun on 9/20/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

#import "ImagesViewController.h"

// Views
#import "GettyImageCollectionViewCell.h"
#import "CaptionView.h"

// Managers
#import "DataFetchManager.h"

// Models
#import "GettyImage.h"

// Presenters
#import "GettyImagePresenter.h"

// Categories
#import "UIColor+Branding.h"

static int const kPageSize = 100;

static NSString * const kSearchResource = @"https://api.gettyimages.com:443/v3/search/images?";

@interface ImagesViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate>

@property (nonatomic, weak) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSArray<GettyImage *> *gettyImagesArray;
@property (nonatomic, strong) NSString *searchTerm;
@property (nonatomic, strong) UIImageView *logoImageView;
@property (nonatomic) NSUInteger currentPage;
@property (nonatomic) BOOL isPageRefreshing;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UIBarButtonItem *searchBarButtonItem;
@property (nonatomic, strong) UIRefreshControl *refreshControl;
@property (nonatomic, strong) CaptionView *captionView;

@end

@implementation ImagesViewController

#pragma mark - UIViewController lifecycle methods

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setupRefreshControl];
    [self setupSearchBar];
    [self setupSearchBarButtonItem];
    [self setupNavigationBar];
    [self setupCollectionView];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.navigationItem.titleView = self.logoImageView;
}

#pragma mark - Setting up methods

- (void)setupNavigationBar {
    self.logoImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 60.0f, 20.0f)];
    self.logoImageView.contentMode = UIViewContentModeScaleAspectFit;
    self.logoImageView.image = [UIImage imageNamed:@"logo_icon"];
    
    self.navigationController.navigationBar.barTintColor = [UIColor getty_blue];
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)setupCollectionView {
    self.searchTerm = @"mobile";
    self.currentPage = 1;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([GettyImageCollectionViewCell class]) bundle:nil];
    [self.collectionView registerNib:nib forCellWithReuseIdentifier:NSStringFromClass([GettyImageCollectionViewCell class])];
}

- (void)setupRefreshControl {
    self.refreshControl = [UIRefreshControl new];
    self.refreshControl.tintColor = [UIColor whiteColor];
    [self.collectionView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(fetchData) forControlEvents:UIControlEventValueChanged];
}

- (void)setupSearchBar {
    self.searchBar = [UISearchBar new];
    self.searchBar.delegate = self;
    self.searchBar.tintColor = [UIColor blackColor];
}

- (void)setupSearchBarButtonItem {
    self.searchBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"search_icon"]
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(openSearchBar)];
    self.searchBarButtonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = self.searchBarButtonItem;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 30.0f, 30.0f)]];
}

#pragma mark - UISearchBar delegate methods

- (void)openSearchBar {
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Cancel", nil)
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:@selector(closeSearchBar)];
    
    self.navigationItem.rightBarButtonItem.tintColor = [UIColor blackColor];
    
    self.navigationItem.titleView = self.searchBar;
    self.searchBar.alpha = 0.0f;
    [self.searchBar becomeFirstResponder];
    
    [UIView animateWithDuration:0.5f
                     animations:^{
                         self.searchBar.alpha = 1.0f;
                     } completion:nil];
}

- (void)closeSearchBar {
    self.searchBar.text = @"";
    self.navigationItem.rightBarButtonItem = nil;
    
    [UIView animateWithDuration:0.5f animations:^{
        self.searchBar.alpha = 0.0f;
    } completion:^(BOOL finished) {
        self.navigationItem.titleView = nil;
        self.navigationItem.rightBarButtonItem = self.searchBarButtonItem;
        self.navigationItem.titleView = self.logoImageView;
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 30.0f, 30.0f)]];
    }];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    self.searchTerm = searchBar.text;
    self.currentPage = 0;
    
    [self fetchData];
    
    [searchBar resignFirstResponder];
}

#pragma mark - UICollectionView delegate methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.gettyImagesArray.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat sideLength = CGRectGetWidth(self.view.frame) / 2;
    return CGSizeMake(sideLength, sideLength);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GettyImageCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([GettyImageCollectionViewCell class]) forIndexPath:indexPath];
    
    GettyImage *gettyImage = self.gettyImagesArray[indexPath.item];
    
    GettyImagePresenter *presenter = [[GettyImagePresenter alloc] initWithModel:gettyImage
                                                                           view:cell];
    [presenter updateView];
    
    return cell;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionView *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0f;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0.0f;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    GettyImage *gettyImage = self.gettyImagesArray[indexPath.item];
    [self presentPopupWithCaption:gettyImage];
}

- (void)presentPopupWithCaption:(GettyImage *)gettyImage {
    if ([gettyImage.caption isKindOfClass:[NSString class]]) {
        if (self.captionView) {
            [self.captionView removeFromSuperview];
        }
        
        self.captionView = [[CaptionView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 250.0f, 60.0f)];
        self.captionView.center = self.view.superview.center;
        self.captionView.text = gettyImage.caption;
        self.captionView.layer.shadowOpacity = 0.8f;
        self.captionView.layer.shadowOffset = CGSizeMake(0.0f, 0.0f);
        
        [UIView animateWithDuration:0.4f animations:^{
            self.captionView.alpha = 1.0f;
        }];
        
        [self.view addSubview:self.captionView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.collectionView.contentOffset.y >= (self.collectionView.contentSize.height -
                                                self.collectionView.bounds.size.height)) {
        
        if (self.isPageRefreshing) {
            return;
        }
        
        self.isPageRefreshing = YES;
        
        [[DataFetchManager sharedInstance] loadGettyImages:[self getNextPageAddress]
                                         completionHandler:^(NSArray *gettyImages, NSError *error) {
                                             [self.collectionView performBatchUpdates:^{
                                                 NSUInteger previousSize = self.gettyImagesArray.count;
                                                 self.gettyImagesArray = [self.gettyImagesArray arrayByAddingObjectsFromArray:gettyImages];
                                                 NSUInteger currentSize = self.gettyImagesArray.count;
                                                 
                                                 [self updateCollectionViewWithPreviousSize:previousSize currentSize:currentSize];
                                             } completion:^(BOOL finished) {
                                                 self.isPageRefreshing = NO;
                                             }];
                                         }];
    }
}

#pragma mark - Helper methods

- (void)updateCollectionViewWithPreviousSize:(NSUInteger)previousSize currentSize:(NSUInteger)currentSize {
    NSMutableArray<NSIndexPath *> *indexPathsArray = [NSMutableArray new];
    for (NSUInteger i = previousSize; i < currentSize; ++i) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPathsArray addObject:indexPath];
    }
    
    [self.collectionView insertItemsAtIndexPaths:indexPathsArray];
}

- (NSString *)getNextPageAddress {
    NSString *address = [NSString stringWithFormat:@"%@page=%lu&page_size=%lu&phrase=%@",
                         kSearchResource,
                         (unsigned long)++self.currentPage,
                         (unsigned long)kPageSize,
                         self.searchTerm];
    
    NSString *encodedAddress = [address stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    return encodedAddress;
}

- (void)fetchData {
    [[DataFetchManager sharedInstance] loadGettyImages:[self getNextPageAddress]
                                     completionHandler:^(NSArray *gettyImages, NSError *error) {
                                         self.gettyImagesArray = gettyImages;
                                         [self.collectionView reloadData];
                                         
                                         if (self.refreshControl.isRefreshing) {
                                             [self.refreshControl endRefreshing];
                                         }
                                     }];
}

@end