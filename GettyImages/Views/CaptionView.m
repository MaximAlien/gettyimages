//
//  CaptionView.m
//  GettyImages
//
//  Created by Maxim Makhun on 9/23/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

#import "CaptionView.h"

@interface CaptionView ()

@property (nonatomic, strong) UILabel *captionLabel;

@end

@implementation CaptionView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        self.alpha = 0.0f;
        self.numberOfLines = 0;
        self.font = [UIFont systemFontOfSize:12.0f];
        
        [NSTimer scheduledTimerWithTimeInterval:5.0f
                                         target:self
                                       selector:@selector(hideCaptionView)
                                       userInfo:nil
                                        repeats:NO];
    }
    
    return self;
}

- (void)setText:(NSString *)text {
    [super setText:text];
    [self adjustHeight];
}

- (void)adjustHeight {
    if (!self.text) {
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, self.bounds.size.width, 0.0f);
        return;
    }
    
    CGSize size = self.bounds.size;
    CGSize tmpSize = CGRectInfinite.size;
    tmpSize.width = size.width;
    
    tmpSize = [self.text boundingRectWithSize:CGSizeMake(self.frame.size.width, MAXFLOAT)
                                      options:NSStringDrawingUsesLineFragmentOrigin
                                   attributes:@{NSFontAttributeName : self.font}
                                      context:nil].size;
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, size.width, tmpSize.height);
}

- (void)hideCaptionView {
    [UIView animateWithDuration:0.4f animations:^{
        self.alpha = 1.0f;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
