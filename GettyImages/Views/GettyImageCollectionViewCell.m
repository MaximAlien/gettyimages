//
//  GettyImageCollectionViewCell.m
//  GettyImages
//
//  Created by Maxim Makhun on 9/20/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

#import "GettyImageCollectionViewCell.h"

@implementation GettyImageCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self styleCollectionViewCell];
}

- (void)prepareForReuse {
    self.imageView.image = nil;
    self.titleLabel.text = @"";
}

- (void)styleCollectionViewCell {
    self.layoutMargins = UIEdgeInsetsZero;
    self.backgroundColor = [UIColor whiteColor];
    
    self.titleLabel.font = [UIFont fontWithName:@"Arial" size:12.0f];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.backgroundColor = [UIColor blackColor];
}

@end
