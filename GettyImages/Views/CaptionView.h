//
//  CaptionView.h
//  GettyImages
//
//  Created by Maxim Makhun on 9/23/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

@import UIKit;

@interface CaptionView : UILabel

@end
