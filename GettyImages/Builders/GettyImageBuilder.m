//
//  GettyImageBuilder.m
//  GettyImages
//
//  Created by Maxim Makhun on 9/22/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

#import "GettyImageBuilder.h"

// Models
#import "GettyImage.h"

@implementation GettyImageBuilder

#pragma mark - Parser methods

+ (NSArray *)gettyImagesFromJSON:(NSData *)data error:(NSError **)error {
    if (!data) {
        return nil;
    }
    
    NSError *localError;
    NSArray *results = [NSJSONSerialization JSONObjectWithData:data
                                                       options:NSJSONReadingMutableContainers
                                                         error:&localError];
    if (localError) {
        *error = localError;
        return nil;
    }
    
    NSMutableArray *gettyImages = [NSMutableArray new];
    NSArray *gettyImagesArray = [results valueForKey:@"images"];
    
    if (gettyImagesArray != (id)[NSNull null]) {
        for (NSDictionary *gettyImageDict in gettyImagesArray) {
            GettyImage *gettyImage = [GettyImage new];
            
            for (NSString *key in gettyImageDict) {
                @try {
                    id value = [gettyImageDict valueForKey:key];
                    
                    if ([key isEqualToString:@"id"]) {
                        [gettyImage setValue:value forKey:@"ID"];
                    } else if ([key isEqualToString:@"display_sizes"]) {
                        for (NSDictionary *displaySizes in value) {
                            NSString *name = [displaySizes valueForKey:@"name"];
                            
                            if ([name isEqualToString:@"thumb"]) {
                                NSString *uri = [displaySizes valueForKey:@"uri"];
                                [gettyImage setValue:uri forKey:@"uri"];
                                break;
                            }
                        }
                    } else {
                        if ([gettyImage respondsToSelector:NSSelectorFromString(key)]) {
                            [gettyImage setValue:value forKey:key];
                        }
                    }
                } @catch (NSException *exception) {
                    NSLog(@"Exception occured in %@: %s", exception, __func__);
                    continue;
                }
            }
            
            [gettyImages addObject:gettyImage];
        }
    }
    
    return gettyImages;
}

@end
