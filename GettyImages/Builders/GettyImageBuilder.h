//
//  GettyImageBuilder.h
//  GettyImages
//
//  Created by Maxim Makhun on 9/22/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

@import Foundation;

@interface GettyImageBuilder : NSObject

+ (NSArray *)gettyImagesFromJSON:(NSData *)data error:(NSError **)error;

@end
