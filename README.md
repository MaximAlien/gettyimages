GettyImages
===================================

**Supported iOS versions:** 8.0 - 9.3  
**Supported devices:** iPhone only

##GettyImages project consists of three targets (schemes):
- GettyImages - actual application (this target does not contain unit tests).
- GettyImagesTests - set of unit tests (can only be started as Test target).
- GettyImagesTestHost - application which works as a base for all unit tests. Allows to simplify process of running unit tests in clean environment.