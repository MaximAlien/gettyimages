//
//  AppDelegate.h
//  GettyImagesTestHost
//
//  Created by Maxim Makhun on 9/23/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

