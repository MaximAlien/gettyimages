//
//  main.m
//  GettyImagesTestHost
//
//  Created by Maxim Makhun on 9/23/16.
//  Copyright © 2016 Maxim Makhun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
